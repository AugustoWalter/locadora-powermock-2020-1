package br.ucsal.testequalidade20192.locadora.builder;

import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String PLACA_DEFAULT = "ABC-1234";
	private static final Integer ANO_DEFAULT = 2000;
	private static final Modelo MODELO_DEFAULT = null;
	private static final Double VALOR_DIARIA_DEFAULT = 90.6;
	private static final SituacaoVeiculoEnum  SITUACAU_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL; 


	private String placa = PLACA_DEFAULT;
	private Integer ano = ANO_DEFAULT;
	private Modelo modelo = MODELO_DEFAULT; 
	private Double valorDiaria = VALOR_DIARIA_DEFAULT;
	private SituacaoVeiculoEnum situacao = SITUACAU_DEFAULT;


	private VeiculoBuilder() {

	}

	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}

	public static VeiculoBuilder umVeiculoDisponível() {
		return new VeiculoBuilder().disponivel();
	}


	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder manutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder mas() {
		return umVeiculo().comPlaca(placa).comAno(ano).comModelo(modelo).comValorDiaria(valorDiaria).comSituacao(situacao);
	}

	public Veiculo build() {

		Veiculo veiculo = new Veiculo(placa, ano, modelo, valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;
	}

}












