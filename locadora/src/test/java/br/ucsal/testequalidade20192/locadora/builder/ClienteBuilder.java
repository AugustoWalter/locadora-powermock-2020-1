package br.ucsal.testequalidade20192.locadora.builder;

import br.ucsal.testequalidade20192.locadora.dominio.Cliente;

public class ClienteBuilder {

	private static final String CPF_DEFAULT = "000000000272";
	private static final String NOME_DEFAULT = "Maria da Silva";
	private static final String TELEFONE_DEFAULT = "719988776655";
	
	private String cpf = CPF_DEFAULT;
	private String nome = NOME_DEFAULT;
	private String telefone = TELEFONE_DEFAULT;

 
	private ClienteBuilder() {
		
	}
   
	public static ClienteBuilder umCliente() {
		return new ClienteBuilder();
	}
	

	public ClienteBuilder comCpf(String cpf) {
		this.cpf = cpf;
		return this;
	}
	public ClienteBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public ClienteBuilder comTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}
	
	public ClienteBuilder mas() {
		return umCliente().comCpf(cpf).comNome(nome).comTelefone(telefone);
	}
	
	public Cliente build() {
		return new Cliente(cpf, nome, telefone);
	}
	
}
