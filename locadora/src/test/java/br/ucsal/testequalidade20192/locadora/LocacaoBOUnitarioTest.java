package br.ucsal.testequalidade20192.locadora;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.testequalidade20192.locadora.builder.ClienteBuilder;
import br.ucsal.testequalidade20192.locadora.builder.VeiculoBuilder;
import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ClienteDAO.class, VeiculoDAO.class, LocacaoDAO.class, LocacaoBO.class})
/**
 * 1. No @PrepareForTest, foi necessário incluir , LocacaoBO.class (pois nela está sendo feita as instanciações, ou  seja, está sendo utilizado o  new);
 * @author Augusto Walter
 *
 */
public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um veículo disponível para um cliente cadastrado, um
	 * contrato de locação é inserido.
	 * 
	 * Método:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observação1: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observação2: lembre-se de que o método locarVeiculos é um método command.
	 * 
	 * @throws Exception
	 */
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {

		String cpfCliente = "000000000353";
		String placa = "XYZ-1234";
		List<String> placas = Arrays.asList(placa);
		Date dataLocacao = new Date();
		Integer quantidadeDiasLocacao = 12;

		PowerMockito.mockStatic(ClienteDAO.class);
		Cliente cliente1 = ClienteBuilder.umCliente().comCpf(cpfCliente).build();
		PowerMockito.when(ClienteDAO.obterPorCpf(cpfCliente)).thenReturn(cliente1);

		PowerMockito.mockStatic(VeiculoDAO.class);
		Veiculo veiculo1 = VeiculoBuilder.umVeiculoDisponível().comPlaca(placa).build();
		PowerMockito.when(VeiculoDAO.obterPorPlaca(placa)).thenReturn(veiculo1);

		/**
		 * (instância de arraylist que será retornada ao mocar o new em List<Veiculo> veiculos = new ArrayList<>(); linha 60 de LocacaoBO)
		 */
		ArrayList<Veiculo> veiculos = new ArrayList<>();

		/**
		 * (instância de Locacao que será retornada ao mocar o new em Locacao locacao = new Locacao(cliente, veiculos, dataLocacao, quantidadeDiasLocacao); 
		 * linha 69 de LocacaoBO)
		 */
		Locacao locacao1 = new Locacao(cliente1, veiculos, dataLocacao, quantidadeDiasLocacao);

		PowerMockito.whenNew(ArrayList.class).withNoArguments().thenReturn(veiculos);
		PowerMockito.whenNew(Locacao.class).withArguments(cliente1, veiculos, dataLocacao, quantidadeDiasLocacao).thenReturn(locacao1);


		PowerMockito.mockStatic(LocacaoDAO.class);

		LocacaoBO.locarVeiculos(cpfCliente, placas, dataLocacao, quantidadeDiasLocacao);

		/**
		 *  Substituir o "LocacaoDAO.insert(Mockito.any(Locacao.class));" por "LocacaoDAO.insert(locacao1);", 
		 *  na linha 74 do LocacaoBOUnitarioTest. Desta forma, garantimos que a instância de locação que será persistida 
		 *  é exatamente a que mocamos no processo de instanciação. Se, durante a manutenção do código, alguém modificar, por exemplo, 
		 *  a instância de cliente do processo de locação (exemplo de problema que levou à essa nova implementação de teste), 
		 *  a nova forma de verificar vai pegar.
		 */
		PowerMockito.verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao1);
		PowerMockito.verifyNoMoreInteractions(LocacaoDAO.class);


	}
}
